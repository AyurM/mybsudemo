import { Fragment } from "react";
import Head from "next/head";
import AuthWrapper from "../../components/auth/AuthWrapper";
import TimetableScreen from "../../components/timetable/TimetableScreen";

const TimetablePage = () => {
  return (
    <Fragment>
      <Head>
        <title>Расписание</title>
      </Head>
      <AuthWrapper WrappedComponent={TimetableScreen} />
    </Fragment>
  );
};

export default TimetablePage;

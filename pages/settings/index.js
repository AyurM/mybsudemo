import { Fragment } from "react";
import Head from "next/head";
import AuthWrapper from "../../components/auth/AuthWrapper";
import SettingsScreen from "../../components/settings/SettingsScreen";

const SettingsPage = () => {
  return (
    <Fragment>
      <Head>
        <title>Настройки</title>
      </Head>
      <AuthWrapper WrappedComponent={SettingsScreen} />
    </Fragment>
  );
};

export default SettingsPage;

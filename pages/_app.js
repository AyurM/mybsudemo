import { useEffect } from "react";
import { Provider } from "react-redux";
import store from "../store/index";
import Layout from "../components/layout/Layout";

import "../styles/globals.css";

function MyApp({ Component, pageProps }) {
  useEffect(() => {
    document.body.className =
      localStorage.getItem("darkTheme") == 1 ? "dark-theme" : "";
  }, []);

  return (
    <Provider store={store}>
      <div id="mobile-menu-portal"></div>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </Provider>
  );
}

export default MyApp;

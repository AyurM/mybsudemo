import { Fragment } from "react";
import Head from "next/head";
import AuthWrapper from "../components/auth/AuthWrapper";
import HomeScreen from "../components/home/HomeScreen";

const HomePage = () => {
  return (
    <Fragment>
      <Head>
        <title>Главная страница</title>
      </Head>
      <AuthWrapper WrappedComponent={HomeScreen} />
    </Fragment>
  );
};

export default HomePage;

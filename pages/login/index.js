import { Fragment } from "react";
import Head from "next/head";
import LoginScreen from "../../components/login/LoginScreen";

const LoginPage = () => {
  return (
    <Fragment>
      <Head>
        <title>Вход в личный кабинет</title>
      </Head>
      <LoginScreen />
    </Fragment>
  );
};

export default LoginPage;

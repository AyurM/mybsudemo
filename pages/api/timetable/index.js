import { ObjectId } from "mongodb";
import { connectToDB } from "../../../util/mongodb";
import { formatTimetable } from "../../../util/timetable";

async function handler(req, res) {
  if (req.method === "GET") {
    const teacherId = req.query.teacherId;
    const groupId = req.query.groupId;
    const role = teacherId ? "Преподаватель" : "Студент";
    const id = teacherId ? teacherId : groupId;

    const { db } = await connectToDB();
    const lessonsCollection = db.collection("fulltime_lessons");

    const lessonQueryResult = await lessonsCollection
      .find(getFindOptions(role, id))
      .project(getProjection(role))
      .toArray();

    res.json({
      timetable: formatTimetable(lessonQueryResult, role),
    });
  }
}

const getFindOptions = (role, id) => {
  if (role === "Преподаватель") {
    return {
      teacherId: ObjectId(id),
    };
  }

  if (role === "Студент") {
    return {
      groups: id,
    };
  }
};

const getProjection = (role) => {
  if (role === "Преподаватель") {
    return { teacherId: 0, teacherName: 0, _id: 0 };
  }
  if (role === "Студент") {
    return { _id: 0 };
  }
};

export default handler;

import { connectToDB } from "../../util/mongodb";

const MAX_NEWS = 8;

async function handler(req, res) {
  if (req.method === "GET") {
    const { db } = await connectToDB();
    const newsCollection = db.collection("news");

    const news = await newsCollection
      .find({ type: "news" })
      .limit(MAX_NEWS)
      .toArray();
    const jabber = await newsCollection
      .find({ type: "jabber" })
      .limit(MAX_NEWS)
      .toArray();

    res.json({ news: news, jabber: jabber });
  }
}

export default handler;

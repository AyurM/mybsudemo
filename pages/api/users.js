import { connectToDB } from "../../util/mongodb";

async function handler(req, res) {
  if (req.method === "GET") {
    const { db } = await connectToDB();
    const usersCollection = db.collection("users");

    const startsWith = req.query.startsWith;
    const usersQueryResult = await usersCollection
      .find({ lastName: { $regex: "^" + startsWith, $options: "i" } })
      .toArray();

    const users = usersQueryResult.map((user) => ({
      id: user._id.toString(),
      name:
        user.lastName +
        " " +
        user.name +
        " " +
        (user.patronymic ? user.patronymic : ""),
      position:
        user.position +
        ", " +
        (user.role === "Студент"
          ? "Группа " + user.department
          : user.department),
    }));

    res.json({ users: users });
  }
}

export default handler;

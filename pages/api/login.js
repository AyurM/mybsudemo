import { connectToDB } from "../../util/mongodb";
import { ObjectId } from "mongodb";
import jwt from "jsonwebtoken";

const JWT_SECRET_KEY = "MY_BSU_DEMO_SECRET_KEY";

const handler = async (req, res) => {
  if (req.method === "POST") {
    let token;
    if (req.headers.authorization) {
      token = req.headers.authorization.split(" ")[1];
    }

    const { db } = await connectToDB();
    const usersCollection = db.collection("users");

    if (!token) {
      await authWithLoginAndPassword(req, res, usersCollection);
      return;
    }
    await authWithToken(res, usersCollection, token);
  }
};

const authWithLoginAndPassword = async (req, res, dbCollection) => {
  const { login, password } = req.body;
  if (!login || !password) {
    res.status(401).json({ message: "Ошибка аутентификации" });
    return;
  }
  let user;
  try {
    user = await dbCollection.findOne({ _id: ObjectId(login) });
    if (!user) {
      throw new Error("Неправильный ID пользователя");
    }
  } catch (err) {
    res.status(401).json({ message: "Пользователь не найден" });
    return;
  }

  if (user.password === password) {
    // let newToken;
    // if (getToken) {
    const newToken = jwt.sign(
      { id: user._id.toString(), role: user.role },
      JWT_SECRET_KEY
    );
    // }
    res.json({
      message: "Authenticated with login and password",
      token: newToken,
      user: formatUser(user),
    });
    return;
  }
  res.status(401).json({ message: "Неправильный пароль" });
};

const authWithToken = async (res, dbCollection, token) => {
  try {
    let userTokenInfo = jwt.verify(token, JWT_SECRET_KEY);
    const user = await dbCollection.findOne({
      _id: ObjectId(userTokenInfo.id),
    });

    if (user && user.role === userTokenInfo.role) {
      res.json({
        message: "Authenticated with token",
        user: formatUser(user),
      });
    } else {
      res.status(401).json({ message: "Ошибка аутентификации" });
    }
  } catch (err) {
    res.status(401).json({ message: "Неверный токен аутентификации" });
  }
};

const formatUser = (user) => {
  return {
    id: user._id.toString(),
    name: user.name,
    lastName: user.lastName,
    patronymic: user.patronymic,
    role: user.role,
    department: user.department,
    position: user.position,
    imageUrl: user.imageUrl,
  };
};

export default handler;

import { connectToDB } from "../../util/mongodb";

const getRole = (queryParamValue) => {
  if (queryParamValue === "t") {
    return "Преподаватель";
  }
  return "Студент";
};

const formatLinks = (rawLinks) => {
  const categories = new Set();
  rawLinks.forEach((link) => {
    categories.add(link.category);
  });

  const result = [];
  categories.forEach((category) => {
    const linkGroup = {};
    linkGroup.category = category;
    linkGroup.links = rawLinks
      .filter((rawLink) => rawLink.category === category)
      .map((filteredLink) => ({
        icon: filteredLink.icon,
        title: filteredLink.title,
        link: filteredLink.link,
        order: filteredLink.order,
      }))
      .sort((a, b) => {
        return a.order - b.order;
      });
    result.push(linkGroup);
  });
  return result;
};

async function handler(req, res) {
  if (req.method === "GET") {
    const role = req.query.role;
    const { db } = await connectToDB();
    const navLinksCollection = db.collection("nav_links");

    const rawLinks = await navLinksCollection
      .find({ roles: getRole(role) })
      .project({ _id: 0 })
      .toArray();

    res.json(formatLinks(rawLinks));
  }
}

export default handler;

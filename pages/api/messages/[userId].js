import { connectToDB } from "../../../util/mongodb";
import {
  getInterlocutorsIds,
  buildDialogues,
} from "../../../util/messages/message-utils";
import { ObjectId } from "mongodb";

async function handler(req, res) {
  if (req.method === "GET") {
    const { userId } = req.query;
    const { db } = await connectToDB();

    const messages = await getUserMessages(userId, db.collection("messages"));
    const users = await getInterlocutorsInfo(
      messages,
      userId,
      db.collection("users")
    );

    const result = {};
    result.dialogues = buildDialogues(messages, users);

    res.json(result);
  }
}

const getUserMessages = async (userId, dbCollection) => {
  const messagesQueryResult = await dbCollection
    .find({
      $or: [{ from: ObjectId(userId) }, { to: ObjectId(userId) }],
    })
    .toArray();

  return messagesQueryResult.map((message) => ({
    id: message._id.toString(),
    from: message.from.toString(),
    to: message.to.toString(),
    text: message.text,
    seen: message.seen,
    timestamp: message.timestamp,
  }));
};

const getInterlocutorsInfo = async (messages, userId, dbCollection) => {
  const interlocutorsIds = getInterlocutorsIds(messages, userId);
  const usersQueryResult = await dbCollection
    .find(
      { _id: { $in: interlocutorsIds.map((id) => ObjectId(id)) } },
      { password: 0 }
    )
    .toArray();
  return usersQueryResult.map((user) => ({
    id: user._id.toString(),
    name: user.name,
    lastName: user.lastName,
    patronymic: user.patronymic,
    role: user.role,
    department: user.department,
    position: user.position,
    imageUrl: user.imageUrl,
  }));
};

export default handler;

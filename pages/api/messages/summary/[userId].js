import { connectToDB } from "../../../../util/mongodb";
import {
  getInterlocutorsIds,
  buildMessageSummary,
} from "../../../../util/messages/message-utils";
import { ObjectId } from "mongodb";

async function handler(req, res) {
  if (req.method === "GET") {
    const { userId } = req.query;
    const { db } = await connectToDB();

    const messages = await getNewMessages(userId, db.collection("messages"));
    const users = await getInterlocutorsInfo(
      messages,
      userId,
      db.collection("users")
    );

    const result = buildMessageSummary(messages, users);
    res.json(result);
  }
}

const getNewMessages = async (userId, dbCollection) => {
  const messagesQueryResult = await dbCollection
    .find({
      $and: [{ to: ObjectId(userId) }, { seen: false }],
    })
    .toArray();

  return messagesQueryResult.map((message) => ({
    id: message._id.toString(),
    from: message.from.toString(),
    to: message.to.toString(),
    text: message.text,
    seen: message.seen,
    timestamp: message.timestamp,
  }));
};

const getInterlocutorsInfo = async (messages, userId, dbCollection) => {
  const interlocutorsIds = getInterlocutorsIds(messages, userId);
  const usersQueryResult = await dbCollection
    .find(
      { _id: { $in: interlocutorsIds.map((id) => ObjectId(id)) } },
      { password: 0 }
    )
    .toArray();
  return usersQueryResult.map((user) => ({
    id: user._id.toString(),
    name: user.name,
    lastName: user.lastName,
    patronymic: user.patronymic,
  }));
};

export default handler;

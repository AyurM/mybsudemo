import { Fragment } from "react";
import Head from "next/head";
import AuthWrapper from "../components/auth/AuthWrapper";
import PlaceholderPage from "../components/ui/PlaceholderPage";

const SlugPage = () => {
  return (
    <Fragment>
      <Head>
        <title>Раздел отсутствует</title>
      </Head>
      <AuthWrapper WrappedComponent={PlaceholderPage} />
    </Fragment>
  );
};

export default SlugPage;

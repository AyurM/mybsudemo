import { Fragment } from "react";
import Head from "next/head";
import AuthWrapper from "../../components/auth/AuthWrapper";
import MessageScreen from "../../components/messages/MessageScreen";

const MessagePage = () => {
  return (
    <Fragment>
      <Head>
        <title>Сообщения</title>
      </Head>
      <AuthWrapper WrappedComponent={MessageScreen} />
    </Fragment>
  );
};

export default MessagePage;

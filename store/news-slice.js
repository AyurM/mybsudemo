import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoadingNews: false,
  bsuNews: [],
  jabber: [],
};

const newsSlice = createSlice({
  name: "news",
  initialState: initialState,
  reducers: {
    setNews(state, action) {
      state.bsuNews = action.payload.bsuNews;
      state.jabber = action.payload.jabber;
      state.isLoadingNews = false;
    },
    setLoadingNews(state, action) {
      state.isLoadingNews = action.payload.isLoadingNews;
    },
  },
});

export const newsActions = newsSlice.actions;
export default newsSlice.reducer;

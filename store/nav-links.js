import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  links: [],
};

const navLinksSlice = createSlice({
  name: "navLinks",
  initialState: initialState,
  reducers: {
    setLinks(state, action) {
      state.links = action.payload.links;
    },
  },
});

export const navLinksActions = navLinksSlice.actions;
export default navLinksSlice.reducer;

import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  dialogues: [],
  selectedDialogueIndex: null,
  isLoading: false,
};

const dialogueSlice = createSlice({
  name: "dialogue",
  initialState: initialState,
  reducers: {
    setDialogue(state, action) {
      state.dialogues = action.payload.dialogues;
      state.selectedDialogueIndex =
        action.payload.dialogues.length > 0 ? 0 : null;
      state.isLoading = false;
    },
    clearDialogues(state) {
      state.dialogues = [];
      state.selectedDialogueIndex = null;
      state.isLoading = false;
    },
    setIsLoading(state, action) {
      state.isLoading = action.payload.isLoading;
    },
    selectDialogue(state, action) {
      let index = state.dialogues.findIndex(
        (d) => d.with.id === action.payload.id
      );
      state.selectedDialogueIndex = index;
    },
    sendMessage(state, action) {
      state.dialogues[state.selectedDialogueIndex].messages.push(
        action.payload
      );
    },
  },
});

export const dialogueActions = dialogueSlice.actions;
export default dialogueSlice.reducer;

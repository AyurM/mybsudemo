import { configureStore } from "@reduxjs/toolkit";
import userSliceReducer from "./user-slice";
import newsSliceReducer from "./news-slice";
import messageSummarySliceReducer from "./messages-summary-slice";
import dialogueSliceReducer from "./dialogue-slice";
import timetableSliceReducer from "./timetable-slice";
import authSliceReducer from "./auth-slice";
import uiSliceReducer from "./ui-slice";
import navLinksSliceReducer from "./nav-links";

const store = configureStore({
  reducer: {
    user: userSliceReducer,
    news: newsSliceReducer,
    messageSummary: messageSummarySliceReducer,
    dialogue: dialogueSliceReducer,
    timetable: timetableSliceReducer,
    auth: authSliceReducer,
    ui: uiSliceReducer,
    navLinks: navLinksSliceReducer,
  },
});
export default store;

import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  new: 0,
  from: [],
  totalFrom: 0,
};

const messageSummarySlice = createSlice({
  name: "messageSummary",
  initialState: initialState,
  reducers: {
    setMessageSummary(state, action) {
      state.new = action.payload.new;
      state.from = action.payload.from;
      state.totalFrom = action.payload.totalFrom;
    },
  },
});

export const messageSummaryActions = messageSummarySlice.actions;
export default messageSummarySlice.reducer;

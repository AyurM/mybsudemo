import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  showMobileMenu: false,
};

const uiSlice = createSlice({
  name: "ui",
  initialState: initialState,
  reducers: {
    switchMobileMenu(state, action) {
      state.showMobileMenu = action.payload.show;
    },
  },
});

export const uiActions = uiSlice.actions;
export default uiSlice.reducer;

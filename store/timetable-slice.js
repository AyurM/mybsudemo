import { createSlice } from "@reduxjs/toolkit";

const initialState = { data: [], isLoading: false };

const timetableSlice = createSlice({
  name: "timetable",
  initialState: initialState,
  reducers: {
    setTimetable(state, action) {
      state.data = action.payload.data;
      state.isLoading = false;
    },
    setIsLoading(state, action) {
      state.isLoading = action.payload.isLoading;
    },
  },
});

export const timetableActions = timetableSlice.actions;
export default timetableSlice.reducer;

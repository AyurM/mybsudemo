import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  id: null,
  name: null,
  lastName: null,
  patronymic: null,
  role: null,
  department: null,
  position: null,
  imageUrl: null,
};

const userSlice = createSlice({
  name: "user",
  initialState: initialState,
  reducers: {
    setUser(state, action) {
      state.id = action.payload.id;
      state.name = action.payload.name;
      state.lastName = action.payload.lastName;
      state.patronymic = action.payload.patronymic;
      state.role = action.payload.role;
      state.department = action.payload.department;
      state.position = action.payload.position;
      state.imageUrl = action.payload.imageUrl;
    },
    clearUser(state) {
      state.id = null;
      state.name = null;
      state.lastName = null;
      state.patronymic = null;
      state.role = null;
      state.department = null;
      state.position = null;
      state.imageUrl = null;
    },
  },
});

export const userActions = userSlice.actions;
export default userSlice.reducer;

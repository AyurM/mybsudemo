import { useState, useEffect } from "react";

//https://usehooks.com/useWindowSize/
export const useWindowSize = () => {
  const [windowSize, setWindowSize] = useState(undefined);
  useEffect(() => {
    function handleResize() {
      let size = "large";
      if (window.innerWidth < 600) {
        size = "small";
      } else if (window.innerWidth >= 600 && window.innerWidth < 1400) {
        size = "medium";
      }
      if (size === windowSize) {
        return;
      }
      setWindowSize(size);
    }
    window.addEventListener("resize", handleResize);
    handleResize();
    // Remove event listener on cleanup
    return () => window.removeEventListener("resize", handleResize);
  }, [windowSize]);
  return windowSize;
};

export const getFormattedDate = (
  timestamp,
  hideTime = false,
  shortMonth = false
) => {
  let result, time;
  const nowDate = new Date();
  const date = new Date(timestamp);
  if (!hideTime) {
    time = date.toLocaleTimeString("ru", {
      hour: "2-digit",
      minute: "2-digit",
    });
  }

  nowDate.setHours(0, 0, 0, 0);
  date.setHours(0, 0, 0, 0);

  const yearDiff = nowDate.getFullYear() - date.getFullYear();

  if (yearDiff !== 0) {
    result = getDateWithYear(date, shortMonth);
  }

  if (yearDiff === 0) {
    result = getDateWithoutYear(
      date,
      nowDate.getTime() - date.getTime(),
      shortMonth
    );
  }

  if (!hideTime) {
    result += " в " + time;
  }
  return result;
};

const getDateWithYear = (date, shortMonth) => {
  let result = date.toLocaleDateString("ru-RU", {
    day: "numeric",
    month: shortMonth ? "2-digit" : "short",
    year: "numeric",
  });
  result = result.replace(" г.", "");
  return result;
};

const getDateWithoutYear = (date, dateDifference, shortMonth) => {
  if (dateDifference === 0) {
    return "Сегодня";
  }

  if (dateDifference === 24 * 60 * 60 * 1000) {
    return "Вчера";
  }

  let result = date.toLocaleDateString("ru-RU", {
    day: "numeric",
    month: shortMonth ? "short" : "long",
  });
  if (shortMonth) {
    result = result.replace(".", "");
  }

  return result;
};

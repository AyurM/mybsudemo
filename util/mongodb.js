// https://developer.mongodb.com/how-to/nextjs-with-mongodb/
import { MongoClient } from "mongodb";

const MONGODB_URL = process.env.DATABASE_URL;
const DB_NAME = process.env.DATABASE_NAME;

if (!MONGODB_URL) {
  throw new Error(
    "Please define the MONGODB_URL environment variable inside .env.local"
  );
}

if (!DB_NAME) {
  throw new Error(
    "Please define the DB_NAME environment variable inside .env.local"
  );
}

let cached = global.mongo;

if (!cached) {
  cached = global.mongo = { conn: null, promise: null };
}

export async function connectToDB() {
  if (cached.conn) {
    return cached.conn;
  }

  if (!cached.promise) {
    const opts = {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    };

    cached.promise = MongoClient.connect(MONGODB_URL, opts).then((client) => {
      return {
        client,
        db: client.db(DB_NAME),
      };
    });
  }
  cached.conn = await cached.promise;
  return cached.conn;
}

export const MAX_FROM_LENGTH = 3;

export const getInterlocutorsIds = (messages, userId) => {
  let result = new Set();
  messages.forEach((message) => {
    if (message.from !== userId) {
      result.add(message.from);
    }
    if (message.to !== userId) {
      result.add(message.to);
    }
  });
  return [...result];
};

export const buildDialogues = (messages, users) => {
  let result = [];
  users.forEach((user) => {
    let dialogue = {};
    dialogue.with = { ...user };
    dialogue.messages = messages.filter(
      (message) => message.from === user.id || message.to === user.id
    );
    dialogue.messages.sort((a, b) => {
      return new Date(a.timestamp) - new Date(b.timestamp);
    });
    result.push(dialogue);
  });
  result.sort((a, b) => {
    return (
      new Date(b.messages[b.messages.length - 1].timestamp) -
      new Date(a.messages[a.messages.length - 1].timestamp)
    );
  });

  return result;
};

export const buildMessageSummary = (messages, interlocutors) => {
  let result = { new: 0, from: [], totalFrom: 0 };

  result.new = messages.length;
  result.totalFrom = interlocutors.length;

  for (let i = 0; i < Math.min(MAX_FROM_LENGTH, interlocutors.length); i++) {
    result.from.push(interlocutors[i]);
  }

  return result;
};

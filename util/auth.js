export const loginWithToken = async (token) => {
  const response = await fetch("/api/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  });
  const authData = await response.json();
  if (response.status === 401) {
    throw new Error(authData.message);
  }
  if (authData.token) {
    localStorage.setItem("token", authData.token);
  }
  console.log(authData);
  return authData;
};

export const loginWithLoginAndPassword = async (formData) => {
  const response = await fetch("/api/login", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      login: formData.login,
      password: formData.password,
    }),
  });
  const authData = await response.json();
  if (response.status === 401) {
    throw new Error(authData.message);
  }
  if (formData.remember) {
    localStorage.setItem("token", authData.token);
  }
  console.log(authData);
  return authData;
};

export const WEEKS = 2;
export const DAYS = 6;
export const LESSONS = 5;
export const lessonTimeToOrder = new Map([
  [1, "8:00"],
  [2, "9:40"],
  [3, "11:20"],
  [4, "13:00"],
  [5, "14:40"],
  [6, "16:20"],
]);
export const daysOfWeek = new Map([
  [1, "Понедельник"],
  [2, "Вторник"],
  [3, "Среда"],
  [4, "Четверг"],
  [5, "Пятница"],
  [6, "Суббота"],
]);

export const formatTimetable = (lessonsArray, userRole) => {
  if (!lessonsArray || lessonsArray.length === 0) {
    return [];
  }

  let result = [];

  for (let weekNumber = 1; weekNumber <= WEEKS; weekNumber++) {
    const weekResult = getWeekLessons(lessonsArray, weekNumber, userRole);
    if (weekResult) {
      result.push(weekResult);
    }
  }

  return result;
};

const getWeekLessons = (fullTimetable, weekNumber, userRole) => {
  const weekLessons = fullTimetable.filter(
    (lesson) => lesson.week === weekNumber
  );

  if (weekLessons.length === 0) {
    return null;
  }

  let weekResult = { week: weekNumber };
  weekResult.timetable = [];

  for (let dayNumber = 1; dayNumber <= DAYS; dayNumber++) {
    const dayResult = getDayLessons(weekLessons, dayNumber, userRole);
    if (dayResult) {
      weekResult.timetable.push(dayResult);
    }
  }
  return weekResult;
};

const getDayLessons = (weeklyTimetable, dayNumber, userRole) => {
  const dayLessons = weeklyTimetable.filter(
    (lesson) => lesson.day === dayNumber
  );
  if (dayLessons.length === 0) {
    return null;
  }

  let dayResult = { day: dayNumber };
  dayLessons.sort((a, b) => {
    return a.time - b.time;
  });
  dayResult.lessons = dayLessons.map((lesson) =>
    formatLessonItem(lesson, userRole)
  );
  return dayResult;
};

const formatLessonItem = (rawLessonData, userRole) => {
  let result = {
    time: lessonTimeToOrder.get(rawLessonData.time),
    subject: rawLessonData.subject,
    type: rawLessonData.type,
    room: rawLessonData.room,
  };
  if (userRole === "Преподаватель") {
    result.groups = rawLessonData.groups;
  } else {
    result.teacherName = rawLessonData.teacherName;
  }
  return result;
};

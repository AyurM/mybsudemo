import { useCallback, useEffect, useState, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useRouter } from "next/router";

import { loginWithToken } from "../../util/auth";
import LoadingIndicator from "../ui/LoadingIndicator";

import { authActions } from "../../store/auth-slice";
import { userActions } from "../../store/user-slice";

const AuthWrapper = ({ WrappedComponent, ...props }) => {
  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);
  const dispatch = useDispatch();
  const router = useRouter();
  const [verified, setVerified] = useState(false);
  const isUnmounted = useRef(false);

  const sendAuthRequest = useCallback(async () => {
    if (isLoggedIn) {
      return;
    }
    try {
      const token = localStorage.getItem("token");
      const authData = await loginWithToken(token);
      dispatch(authActions.login({ token: token }));
      dispatch(userActions.setUser(authData.user));
      if (!isUnmounted.current) {
        setVerified(true);
      }
    } catch (err) {
      console.log(err.message);
      if (!isUnmounted.current) {
        setVerified(false);
      }
      router.replace("/login");
    }
  }, [dispatch, isLoggedIn, router]);

  useEffect(() => {
    sendAuthRequest();
  }, [sendAuthRequest]);

  useEffect(() => {
    return () => {
      isUnmounted.current = true;
    };
  }, []);

  if (verified || isLoggedIn) {
    return <WrappedComponent {...props} />;
  } else {
    //   Надпись появляется на бэкенде
    // console.log("not verified");
    return (
      <div style={{ display: "grid", placeContent: "center" }}>
        <LoadingIndicator />
      </div>
    );
  }
};

export default AuthWrapper;

import { Fragment, useState } from "react";
import { useSelector } from "react-redux";

import NavigationPanel from "../navigation/NavigationPanel";
import Header from "../navigation/Header";
import LoadingIndicator from "../ui/LoadingIndicator";

import classes from "./Layout.module.css";

const Layout = (props) => {
  const [isLoading, setIsLoading] = useState(false);
  const user = useSelector((state) => state.user);
  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);

  return (
    <div
      className={
        isLoading || !isLoggedIn ? classes.centered : classes["layout-grid"]
      }
    >
      {isLoading && <LoadingIndicator />}
      {!isLoading && isLoggedIn && user.id && (
        <Fragment>
          <NavigationPanel className={classes["layout-nav"]} />
          <Header className={classes["layout-header"]} user={user} />
          <main>{props.children}</main>
        </Fragment>
      )}
      {!isLoggedIn && !isLoading && <main>{props.children}</main>}
    </div>
  );
};

export default Layout;

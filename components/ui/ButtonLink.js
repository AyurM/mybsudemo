import Link from "next/link";

import classes from "./ButtonLink.module.css";

const ButtonLink = (props) => {
  return (
    <Link href={props.link}>
      <a className={classes["button-link"]}>{props.children}</a>
    </Link>
  );
};

export default ButtonLink;

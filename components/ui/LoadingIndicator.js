import classes from "./LoadingIndicator.module.css";

const LoadingIndicator = () => {
  return (
    <div className={classes["loadingio-spinner-spin-eh7ddtk38gh"]}>
      <div className={classes["ldio-wlrv42cln8f"]}>
        <div>
          <div></div>
        </div>
        <div>
          <div></div>
        </div>
        <div>
          <div></div>
        </div>
        <div>
          <div></div>
        </div>
        <div>
          <div></div>
        </div>
        <div>
          <div></div>
        </div>
        <div>
          <div></div>
        </div>
        <div>
          <div></div>
        </div>
      </div>
    </div>
  );
};

export default LoadingIndicator;

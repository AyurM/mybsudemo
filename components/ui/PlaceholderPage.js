/* eslint-disable @next/next/no-img-element */
import classes from "./PlaceholderPage.module.css";

const PlaceholderPage = () => {
  return (
    <div className={classes["placeholder-layout"]}>
      <img src="./images/page_not_ready.png" alt="Возмущенный пользователь" />
      <h2>Страница отсутствует</h2>
      <p>
        Реализованы разделы &quot;Главная&quot;, &quot;Сообщения&quot;,
        &quot;Расписание&quot;, &quot;Настройки&quot;
      </p>
    </div>
  );
};

export default PlaceholderPage;

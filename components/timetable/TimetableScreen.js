import { useEffect, useCallback, useState } from "react";
import { useSelector, useDispatch } from "react-redux";

import Card from "../ui/Card";
import Timetable from "./Timetable";
import TimetableHeader from "./TimetableHeader";

import { timetableActions } from "../../store/timetable-slice";
import classes from "./TimetableScreen.module.css";

const TimetableScreen = () => {
  const user = useSelector((state) => state.user);
  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);
  const fullTimetable = useSelector((state) => state.timetable.data);
  const [weekTimetable, setWeekTimetable] = useState([]);
  const dispatch = useDispatch();

  const fetchTimetable = useCallback(async () => {
    if (!isLoggedIn) {
      return;
    }
    try {
      dispatch(timetableActions.setIsLoading({ isLoading: true }));
      let response, fetchedTimetable;
      if (user.role === "Преподаватель") {
        response = await fetch(`/api/timetable?teacherId=${user.id}`);
      } else {
        response = await fetch(`/api/timetable?groupId=${user.department}`);
      }
      fetchedTimetable = await response.json();
      console.log(fetchedTimetable);
      dispatch(
        timetableActions.setTimetable({ data: fetchedTimetable.timetable })
      );
    } catch (err) {
      console.log(err);
      dispatch(timetableActions.setIsLoading({ isLoading: false }));
    }
  }, [dispatch, user, isLoggedIn]);

  useEffect(() => {
    fetchTimetable();
  }, [fetchTimetable]);

  useEffect(() => {
    const filterResult = fullTimetable.find((timetable) => timetable.week == 1);
    if (filterResult) {
      setWeekTimetable(filterResult.timetable ? filterResult.timetable : []);
    }
  }, [fullTimetable]);

  const onWeekNumberChange = (weekNumber) => {
    const filterResult = fullTimetable.find(
      (timetable) => timetable.week == weekNumber
    );
    if (filterResult) {
      setWeekTimetable(filterResult.timetable ? filterResult.timetable : []);
    }
  };

  return (
    <Card className={classes["timetable-card"]}>
      <div className={classes["timetable-screen-grid"]}>
        <TimetableHeader onWeekNumberChange={onWeekNumberChange} />
        {weekTimetable.length > 0 ? (
          <Timetable timetable={weekTimetable} />
        ) : (
          <div className={classes.centered}>
            <p>Расписание отсутствует</p>
          </div>
        )}
      </div>
    </Card>
  );
};

export default TimetableScreen;

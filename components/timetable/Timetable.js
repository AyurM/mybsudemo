import { useWindowSize } from "../../hooks/useWindowSize";
import FullSizeTimetable from "./FullSizeTimetable";
import MediumSizeTimetable from "./MediumSizeTimetable";
import SmallSizeTimetable from "./SmallSizeTimetable";

import classes from "./Timetable.module.css";

const Timetable = ({ timetable }) => {
  const size = useWindowSize();

  if (size === "large") {
    return <FullSizeTimetable timetable={timetable} />;
  } else if (size === "medium") {
    return <MediumSizeTimetable timetable={timetable} />;
  }

  return <SmallSizeTimetable timetable={timetable} />;
};

export default Timetable;

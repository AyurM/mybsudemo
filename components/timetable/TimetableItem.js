import { useWindowSize } from "../../hooks/useWindowSize";

import classes from "./TimetableItem.module.css";

const getLessonCssClass = (type) => {
  if (!type) {
    return "";
  }
  if (type === "ЛБ") {
    return classes.lab;
  }
  if (type === "ПР") {
    return classes.practice;
  }
  return classes.lecture;
};

const buildLargeItem = (lesson, style) => {
  return (
    <div
      className={`${classes["timetable-item"]} ${getLessonCssClass(
        lesson.type
      )}`}
      style={style}
    >
      <span className={classes.subject}>{lesson.subject}</span>
      <span className={classes.groups}>
        {lesson.groups ? lesson.groups.join(" ").trim() : lesson.teacherName}
      </span>
      <p className={classes["lesson-info"]}>
        <span className={classes.type}>{lesson.type}</span> {lesson.room}
      </p>
    </div>
  );
};

const buildSmallItem = (lesson, style) => {
  return (
    <div
      className={`${classes["timetable-item-small"]} ${getLessonCssClass(
        lesson.type
      )}`}
      style={style}
    >
      <div className={classes["time-room-container"]}>
        <span className={classes.time}>{lesson.time}</span>
        <span className={classes.room}>{lesson.room}</span>
      </div>

      <div className={classes["subject-room-container"]}>
        <span className={classes.subject}>{lesson.subject}</span>
        <span className={classes.groups}>
          {lesson.groups ? lesson.groups.join(" ").trim() : lesson.teacherName}
        </span>
      </div>

      <span className={classes.type}>{lesson.type}</span>
    </div>
  );
};

const TimetableItem = ({ lesson, style }) => {
  const windowSize = useWindowSize();

  if (!lesson) {
    return <div className={classes["timetable-item"]} style={style}></div>;
  }

  if (windowSize === "large") {
    return buildLargeItem(lesson, style);
  }

  return buildSmallItem(lesson, style);
};

export default TimetableItem;

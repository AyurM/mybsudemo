import SmallTimetableDayItem from "./SmallTimetableDayItem";
import { DAYS } from "../../util/timetable";

import classes from "./SmallSizeTimetable.module.css";

const getTimetableContent = (weekTimetable) => {
  let content = [];

  for (let day = 1; day <= DAYS; day++) {
    let dayTimetable = weekTimetable.find((d) => d.day === day);

    if (dayTimetable) {
      content.push(buildDailyTimetable(dayTimetable));
    }
  }
  return content;
};

const buildDailyTimetable = (dayTimetable) => {
  return (
    <SmallTimetableDayItem dayTimetable={dayTimetable} key={dayTimetable.day} />
  );
};

const SmallSizeTimetable = ({ timetable }) => {
  return (
    <div className={classes["small-timetable-container"]}>
      {getTimetableContent(timetable)}
    </div>
  );
};

export default SmallSizeTimetable;

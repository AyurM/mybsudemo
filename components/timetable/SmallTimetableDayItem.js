/* eslint-disable @next/next/no-img-element */
import { useState } from "react";

import TimetableItem from "./TimetableItem";
import { daysOfWeek } from "../../util/timetable";

import classes from "./SmallTimetableDayItem.module.css";

const SmallTimetableDayItem = ({ dayTimetable }) => {
  const [showLessons, setShowLessons] = useState(true);

  const onTitleClick = () => {
    setShowLessons((state) => {
      return !state;
    });
  };

  return (
    <div className={classes["daily-timetable"]} key={dayTimetable.day}>
      <div className={classes["daily-timetable-title"]} onClick={onTitleClick}>
        <span>{daysOfWeek.get(dayTimetable.day)}</span>
        <img
          src="./icons/arrow-down-blue.svg"
          alt=""
          className={showLessons ? "" : classes.closed}
        />
      </div>
      {showLessons &&
        dayTimetable.lessons.map((lesson) => (
          <TimetableItem key={lesson.time} lesson={lesson} />
        ))}
    </div>
  );
};

export default SmallTimetableDayItem;

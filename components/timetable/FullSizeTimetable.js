import { Fragment } from "react";

import TimetableItem from "./TimetableItem";
import { DAYS, LESSONS, lessonTimeToOrder } from "../../util/timetable";

import classes from "./FullSizeTimetable.module.css";

const getLessonGridArea = (day, lesson) => {
  return `${lesson + 1} / 
      ${day + 1} / 
      ${lesson + 2} / 
      ${day + 2}`;
};

const getTimetableContent = (weekTimetable) => {
  let content = [];

  for (let day = 1; day <= DAYS; day++) {
    let dayTimetable = weekTimetable.find((d) => d.day === day);

    for (let lessonNumber = 1; lessonNumber <= LESSONS; lessonNumber++) {
      let lesson;

      if (dayTimetable) {
        lesson = dayTimetable.lessons.find(
          (l) => l.time === lessonTimeToOrder.get(lessonNumber)
        );
      }

      const gridArea = getLessonGridArea(day, lessonNumber);
      content.push(
        <TimetableItem
          lesson={lesson}
          style={{ gridArea: gridArea }}
          key={`${day}${lessonNumber}`}
        />
      );
    }
  }
  return content;
};

const getDaysOfWeek = () => {
  return (
    <Fragment>
      <div></div>
      <span className={classes.day}>Понедельник</span>
      <span className={classes.day}>Вторник</span>
      <span className={classes.day}>Среда</span>
      <span className={classes.day}>Четверг</span>
      <span className={classes.day}>Пятница</span>
      <span className={classes.day}>Суббота</span>
      <div></div>
    </Fragment>
  );
};

const getLessonTimes = () => {
  let result = [];
  for (let i = 1; i <= LESSONS; i++) {
    const timeSplit = lessonTimeToOrder.get(i).split(":");
    let timeSpan1 = (
      <div className={`${classes.time} ${classes[`t${i}`]}`} key={i}>
        {timeSplit[0]}
        <span className={classes.minute}>{timeSplit[1]}</span>
      </div>
    );
    let timeSpan2 = (
      <div className={`${classes.time} ${classes[`t${i}r`]}`} key={`${i}r`}>
        {timeSplit[0]}
        <span className={classes.minute}>{timeSplit[1]}</span>
      </div>
    );
    result.push(timeSpan1);
    result.push(timeSpan2);
  }
  return result;
};

const FullSizeTimetable = ({ timetable }) => {
  return (
    <div className={classes["timetable-grid"]}>
      {getDaysOfWeek()}
      {getLessonTimes()}
      {getTimetableContent(timetable)}
    </div>
  );
};

export default FullSizeTimetable;

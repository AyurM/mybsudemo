import { useWindowSize } from "../../hooks/useWindowSize";

import classes from "./TimetableHeader.module.css";

const TimetableHeader = ({ onWeekNumberChange }) => {
  const windowSize = useWindowSize();

  const onWeekChange = (event) => {
    onWeekNumberChange(event.target.value);
  };

  return (
    <div className={classes["timetable-header"]}>
      <h2>Расписание</h2>
      <div className={classes["select-container"]}>
        <select>
          <option value="Дневное отделение">
            {windowSize === "small" ? "Дневное" : "Дневное отделение"}
          </option>
          <option value="Заочное отделение">
            {windowSize === "small" ? "Заочное" : "Заочное отделение"}
          </option>
        </select>
        <select onChange={onWeekChange}>
          <option value="1">
            {(windowSize === "small" ? "1 нед." : "1 неделя") +
              (windowSize === "small" ? " (тек.)" : " (текущая)")}
          </option>
          <option value="2">
            {windowSize === "small" ? "2 нед." : "2 неделя"}
          </option>
        </select>
      </div>
    </div>
  );
};

export default TimetableHeader;

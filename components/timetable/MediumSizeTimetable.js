import { useSelector } from "react-redux";

import { DAYS, daysOfWeek } from "../../util/timetable";

import classes from "./MediumSizeTimetable.module.css";

const getTimetableContent = (weekTimetable) => {
  let content = [];

  for (let day = 1; day <= DAYS; day++) {
    let dayTimetable = weekTimetable.find((d) => d.day === day);
    if (dayTimetable) {
      content.push(buildDailyTimetable(dayTimetable));
    }
  }
  return content;
};

const buildDailyTimetable = (dayTimetable) => {
  return (
    <div className={classes["daily-timetable"]} key={dayTimetable.day}>
      <span>{daysOfWeek.get(dayTimetable.day)}</span>
      {dayTimetable.lessons.map((lesson) => (
        <div key={lesson.time} className={classes["lesson-container"]}>
          <span className={classes.time}>{lesson.time}</span>
          <span>{lesson.subject}</span>
          <span className={getLessonCssClass(lesson.type)}>{lesson.type}</span>
          <span>{lesson.room}</span>
          <span>
            {lesson.groups ? lesson.groups.join(" ") : lesson.teacherName}
          </span>
        </div>
      ))}
    </div>
  );
};

const getLessonCssClass = (type) => {
  if (!type) {
    return "";
  }
  if (type === "ЛБ") {
    return classes.lab;
  }
  if (type === "ПР") {
    return classes.practice;
  }
  return classes.lecture;
};

const MediumSizeTimetable = ({ timetable }) => {
  const userRole = useSelector((state) => state.user.role);

  return (
    <div className={classes["medium-timetable-container"]}>
      <div className={classes["timetable-header"]}>
        <span>Время</span>
        <span>Предмет</span>
        <span>Тип</span>
        <span>Ауд.</span>
        <span>{userRole === "Преподаватель" ? "Группа" : "Преподаватель"}</span>
      </div>
      <div>{getTimetableContent(timetable)}</div>
    </div>
  );
};

export default MediumSizeTimetable;

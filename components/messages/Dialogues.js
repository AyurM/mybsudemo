/* eslint-disable @next/next/no-img-element */
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";

import Card from "../ui/Card";
import DialogueItem from "./DialogueItem";

import classes from "./Dialogues.module.css";

const Dialogues = ({ onDialogueSelect, className }) => {
  const dialogueState = useSelector((state) => state.dialogue);
  const [filteredDialogues, setFilteredDialogues] = useState([]);

  useEffect(() => {
    setFilteredDialogues(dialogueState.dialogues);
  }, [dialogueState.dialogues]);

  const onItemSelect = (id) => {
    onDialogueSelect(id);
  };

  const onFilterQueryChange = (event) => {
    const filterQuery = event.target.value.toLowerCase();
    if (filterQuery.trim().length === 0) {
      setFilteredDialogues(dialogueState.dialogues);
      return;
    }

    const filterResult = filteredDialogues.filter(
      (dialogue) =>
        dialogue.with.name.toLowerCase().includes(filterQuery) ||
        dialogue.with.lastName.toLowerCase().includes(filterQuery) ||
        dialogue.with.patronymic.toLowerCase().includes(filterQuery)
    );

    setFilteredDialogues(filterResult);
  };

  let dialoguesContent;
  if (
    !dialogueState.isLoading &&
    (!filteredDialogues || filteredDialogues.length === 0)
  ) {
    dialoguesContent = (
      <div className={classes.centered}>
        <p>Диалоги не найдены</p>
      </div>
    );
  } else if (dialogueState.isLoading) {
    dialoguesContent = (
      <div className={classes.centered}>
        <p>Загрузка...</p>
      </div>
    );
  } else {
    dialoguesContent = (
      <div className={classes["dialogue-list"]}>
        {filteredDialogues.map((fDialogue, index) => (
          <DialogueItem
            key={fDialogue.with.id}
            dialogue={fDialogue}
            onItemSelect={onItemSelect}
            selected={index === dialogueState.selectedDialogueIndex}
          />
        ))}
      </div>
    );
  }

  return (
    <Card className={`${className} ${classes["dialogues"]}`}>
      <div className={classes["dialogue-header"]}>
        <h2>Диалоги</h2>
        <input
          type="search"
          placeholder="Поиск по диалогам"
          onChange={onFilterQueryChange}
        />
      </div>
      {dialoguesContent}
      <button className={classes["new-dialogue-button"]}>
        <div>
          <img src="./icons/plus.svg" alt="" />
          <span>Новый диалог</span>
        </div>
      </button>
    </Card>
  );
};

export default Dialogues;

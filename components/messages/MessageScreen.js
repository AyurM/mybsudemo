import { useState, useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";

import Dialogues from "./Dialogues";
import Chat from "./Chat";

import { dialogueActions } from "../../store/dialogue-slice";
import classes from "./MessageScreen.module.css";

const MessageScreen = () => {
  const [showChat, setShowChat] = useState(false);
  const userId = useSelector((state) => state.user.id);
  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);
  const dialogue = useSelector((state) => state.dialogue);
  const dispatch = useDispatch();

  const fetchMessages = useCallback(async () => {
    if (!isLoggedIn) {
      return;
    }
    try {
      dispatch(dialogueActions.setIsLoading({ isLoading: true }));
      const response = await fetch(`/api/messages/${userId}`);
      const messageData = await response.json();
      console.log(messageData);
      dispatch(
        dialogueActions.setDialogue({ dialogues: messageData.dialogues })
      );
    } catch (err) {
      console.log(err);
      dispatch(dialogueActions.setIsLoading({ isLoading: false }));
    }
  }, [dispatch, userId, isLoggedIn]);

  useEffect(() => {
    fetchMessages();
  }, [fetchMessages]);

  const onDialogueSelect = (id) => {
    dispatch(dialogueActions.selectDialogue({ id: id }));
    setShowChat(true);
  };

  const onBackToDialoguesClick = () => {
    setShowChat(false);
  };

  const onSendMessage = (message) => {
    dispatch(dialogueActions.sendMessage(message));
  };

  return (
    <div className={classes["message-screen-grid"]}>
      <Dialogues
        onDialogueSelect={onDialogueSelect}
        className={showChat ? classes.hide : ""}
      />
      <Chat
        dialogue={dialogue.dialogues[dialogue.selectedDialogueIndex]}
        onSendMessage={onSendMessage}
        onBackToDialoguesClick={onBackToDialoguesClick}
        className={showChat ? "" : classes.hide}
      />
    </div>
  );
};

export default MessageScreen;

import classes from "./ChatMessage.module.css";

const ChatMessage = ({ text, timestamp, incoming }) => {
  const cssClasses = `${classes.message} ${
    incoming ? classes.incoming : classes.outgoing
  }`;

  return (
    <div className={cssClasses}>
      <span>{text}</span>
      <time>
        {new Date(timestamp).toLocaleTimeString("ru-RU", {
          hour: "2-digit",
          minute: "2-digit",
        })}
      </time>
    </div>
  );
};

export default ChatMessage;

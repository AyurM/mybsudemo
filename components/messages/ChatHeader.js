/* eslint-disable @next/next/no-img-element */
import { useWindowSize } from "../../hooks/useWindowSize";

import classes from "./ChatHeader.module.css";

const getInterlocutorFullName = (user) => {
  if (!user) {
    return "";
  }
  return (
    user.lastName +
    " " +
    user.name +
    " " +
    (user.patronymic ? user.patronymic : "")
  );
};

const getInterlocutorShortName = (user) => {
  if (!user) {
    return "";
  }
  return (
    user.lastName +
    " " +
    user.name.charAt(0) +
    ". " +
    (user.patronymic ? user.patronymic.charAt(0) + "." : "")
  );
};

const getUserPosition = (user) => {
  if (!user) {
    return "";
  }
  return (
    user.position +
    ", " +
    (user.role === "Студент" ? "группа " + user.department : user.department)
  );
};

const ChatHeader = ({ user, onBackClick }) => {
  const windowSize = useWindowSize();

  let name;
  if (!user) {
    name = "";
  } else {
    name =
      windowSize === "small"
        ? getInterlocutorShortName(user)
        : getInterlocutorFullName(user);
  }

  return (
    <div className={classes["chat-header"]}>
      <button className={classes["back-to-dialogues"]} onClick={onBackClick}>
        <img src="./icons/left-arrow.svg" alt="" />
        <span>К диалогам</span>
      </button>
      <div className={classes.interlocutor}>
        <img src={user.imageUrl} alt="" />
        <div className={classes["chat-user-info"]}>
          <h2>{name}</h2>
          <span>{getUserPosition(user)}</span>
        </div>
      </div>
    </div>
  );
};

export default ChatHeader;

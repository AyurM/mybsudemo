/* eslint-disable @next/next/no-img-element */
import { useSelector } from "react-redux";

import { getFormattedDate } from "../../util/format-timestamp";

import classes from "./DialogueItem.module.css";

const MAX_MESSAGE_LENGTH = 70;

const getFullName = (userInfo) => {
  return (
    userInfo.lastName +
    " " +
    userInfo.name +
    " " +
    (userInfo.patronymic ? userInfo.patronymic : "")
  );
};

const trimMessage = (message) => {
  if (message.length <= MAX_MESSAGE_LENGTH) {
    return message;
  }

  return message.substring(0, MAX_MESSAGE_LENGTH) + "...";
};

const DialogueItem = ({ dialogue, onItemSelect, selected }) => {
  const userId = useSelector((state) => state.user.id);

  const lastMessage = dialogue.messages[dialogue.messages.length - 1];

  const onClick = () => {
    onItemSelect(dialogue.with.id);
  };

  const newMessages = dialogue.messages.reduce((sum, message) => {
    if (message.to === userId && !message.seen) {
      return ++sum;
    }
    return sum;
  }, 0);

  return (
    <div
      className={`${classes["dialogue-item"]} ${
        selected ? classes.selected : ""
      }`}
      onClick={onClick}
    >
      <img src={dialogue.with.imageUrl} alt="" />

      <div className={classes["dialogue-item-wrapper"]}>
        <div className={classes["dialogue-item-header"]}>
          <span className={classes.name}>{getFullName(dialogue.with)}</span>
          <span className={classes.timestamp}>
            {getFormattedDate(lastMessage.timestamp, true, true)}
          </span>
        </div>
        <div className={classes["dialogue-item-content"]}>
          <p>{trimMessage(lastMessage.text)}</p>
          {newMessages > 0 && (
            <span className={classes["new-messages"]}>{newMessages}</span>
          )}
        </div>
      </div>
    </div>
  );
};

export default DialogueItem;

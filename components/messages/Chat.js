/* eslint-disable @next/next/no-img-element */
import { Fragment, useRef } from "react";
import { useSelector } from "react-redux";

import Card from "../ui/Card";
import ChatHeader from "./ChatHeader";
import ChatMessage from "./ChatMessage";
import { getFormattedDate } from "../../util/format-timestamp";

import classes from "./Chat.module.css";

const getChatContent = (dialogue, userId) => {
  const groupedMessages = groupMessagesByDay(dialogue.messages);
  const content = [];
  groupedMessages.forEach((messages, day) => {
    content.push(
      <div className={classes["day-marker"]} key={day}>
        {day}
      </div>
    );
    content.push(
      messages.map((message, index) => (
        <ChatMessage
          key={index}
          text={message.text}
          timestamp={message.timestamp}
          incoming={message.to === userId}
        />
      ))
    );
  });
  return content;
};

const groupMessagesByDay = (messages) => {
  const groupedMessages = new Map();
  messages.forEach((message) => {
    const day = getFormattedDate(message.timestamp, true);
    if (groupedMessages.has(day)) {
      groupedMessages.get(day).push(message);
    } else {
      groupedMessages.set(day, [message]);
    }
  });
  return groupedMessages;
};

const Chat = (props) => {
  const messageInputRef = useRef();
  const userId = useSelector((state) => state.user.id);

  const onSendMessage = () => {
    if (messageInputRef.current.value.trim().length === 0) {
      return;
    }
    const message = {
      from: userId,
      to: props.dialogue.with.id,
      text: messageInputRef.current.value,
      timestamp: new Date().toISOString(),
    };
    props.onSendMessage(message);
    messageInputRef.current.value = "";
  };

  const onMessageInputKeyPress = (event) => {
    if (event.key === "Enter") {
      onSendMessage();
    }
  };

  const onBackToDialoguesClick = () => {
    props.onBackToDialoguesClick();
  };

  let content;
  if (!props.dialogue) {
    content = <p>Нет выбранного диалога</p>;
  } else {
    content = (
      <Fragment>
        <ChatHeader
          user={props.dialogue ? props.dialogue.with : null}
          onBackClick={onBackToDialoguesClick}
        />
        <div className={classes["chat-area"]}>
          {getChatContent(props.dialogue, userId)}
        </div>
        <div className={classes["chat-controls"]}>
          <img src="./icons/paperclip.svg" alt="" />
          <input
            type="text"
            placeholder="Введите сообщение..."
            ref={messageInputRef}
            onKeyPress={onMessageInputKeyPress}
          />
          <button onClick={onSendMessage}>
            <div>
              <img src="./icons/send.svg" alt="" />
              <span>Отправить</span>
            </div>
          </button>
        </div>
      </Fragment>
    );
  }

  return (
    <Card className={`${props.className} ${classes["chat"]}`}>{content}</Card>
  );
};

export default Chat;

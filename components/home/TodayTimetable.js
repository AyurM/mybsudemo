import Card from "../ui/Card";
import ButtonLink from "../ui/ButtonLink";
import TimetableLesson from "./TimetableLesson";

import classes from "./TodayTimetable.module.css";

const getTodayTimetable = (timetableSlice) => {
  let todayTimetable;
  if (timetableSlice && timetableSlice.data.length > 0) {
    todayTimetable = timetableSlice.data[0].timetable.find(
      (t) => t.day === new Date().getDay()
    );
  }
  return todayTimetable;
};

const getTimetableContent = (timetableSlice) => {
  let timetableContent;
  const todayTimetable = getTodayTimetable(timetableSlice);

  if (timetableSlice.isLoading) {
    timetableContent = <p>Загрузка...</p>;
  } else if (!todayTimetable || todayTimetable.lessons.length === 0) {
    timetableContent = <p>Сегодня занятий нет</p>;
  } else {
    timetableContent = (
      <div className={classes["timetable-container"]}>
        {todayTimetable.lessons.map((lesson) => (
          <TimetableLesson key={lesson.time} lesson={lesson} />
        ))}
      </div>
    );
  }

  return timetableContent;
};

const TodayTimetable = ({ timetable, className }) => {
  return (
    <Card className={`${className} ${classes["timetable-block"]}`}>
      <div className={classes["timetable-flex"]}>
        <h2>Расписание на сегодня</h2>
        <div className={classes.container}>
          {getTimetableContent(timetable)}
          <ButtonLink link="/timetable">К расписанию</ButtonLink>
        </div>
      </div>
    </Card>
  );
};

export default TodayTimetable;

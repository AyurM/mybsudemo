import Card from "../ui/Card";
import NewsItem from "./NewsItem";

import classes from "./NewsGroup.module.css";

const NewsGroup = (props) => {
  let content;
  if (!props.isLoading && (!props.news || props.news.length === 0)) {
    content = (
      <div className={classes.centered}>
        <span>Нет публикаций</span>
      </div>
    );
  } else if (props.isLoading) {
    content = (
      <div className={classes.centered}>
        <span>Загрузка...</span>
      </div>
    );
  } else {
    content = (
      <div className={classes["news-container"]}>
        {props.news.map((item) => (
          <NewsItem post={item} key={item.link} />
        ))}
      </div>
    );
  }

  return (
    <Card className={`${props.className} ${classes["news-group"]}`}>
      <h2>{props.title}</h2>
      {content}
    </Card>
  );
};

export default NewsGroup;

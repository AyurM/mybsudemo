import Link from "next/link";

import { getFormattedDate } from "../../util/format-timestamp";

import classes from "./NewsItem.module.css";

const BASE_URL = "https://bsu.ru/news";
const MAX_TITLE_LENGTH = 110;

const formatPostTitle = (title) => {
  if (title.length > MAX_TITLE_LENGTH) {
    return title.substring(0, MAX_TITLE_LENGTH) + "...";
  }
  return title;
};

const NewsItem = (props) => {
  const postTime = getFormattedDate(props.post.timestamp, true);
  return (
    <Link href={BASE_URL + props.post.link}>
      <a>
        <article className={classes["news-item"]}>
          <h3>{formatPostTitle(props.post.title)}</h3>
          <time>{postTime}</time>
        </article>
      </a>
    </Link>
  );
};

export default NewsItem;

import { useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";

import NewsGroup from "./NewsGroup";
import MessageBlock from "./MessageBlock";
import TodayTimetable from "./TodayTimetable";

import { newsActions } from "../../store/news-slice";
import { messageSummaryActions } from "../../store/messages-summary-slice";
import { timetableActions } from "../../store/timetable-slice";
import { dialogueActions } from "../../store/dialogue-slice";
import classes from "./HomeScreen.module.css";

const HomeScreen = () => {
  const news = useSelector((state) => state.news);
  const messageSummary = useSelector((state) => state.messageSummary);
  const timetable = useSelector((state) => state.timetable);
  const user = useSelector((state) => state.user);
  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);

  const dispatch = useDispatch();

  const fetchNews = useCallback(async () => {
    if (!isLoggedIn) {
      return;
    }
    try {
      dispatch(newsActions.setLoadingNews({ isLoadingNews: true }));
      const response = await fetch("/api/news");
      const newsData = await response.json();
      console.log(newsData);
      dispatch(
        newsActions.setNews({ bsuNews: newsData.news, jabber: newsData.jabber })
      );
    } catch (err) {
      console.log(err);
      dispatch(newsActions.setLoadingNews({ isLoadingNews: false }));
    }
  }, [dispatch, isLoggedIn]);

  const fetchTimetable = useCallback(async () => {
    if (!isLoggedIn) {
      return;
    }
    try {
      dispatch(timetableActions.setIsLoading({ isLoading: true }));
      let response, fetchedTimetable;
      if (user.role === "Преподаватель") {
        response = await fetch(`/api/timetable?teacherId=${user.id}`);
      } else {
        response = await fetch(`/api/timetable?groupId=${user.department}`);
      }
      fetchedTimetable = await response.json();
      console.log(fetchedTimetable);
      dispatch(
        timetableActions.setTimetable({ data: fetchedTimetable.timetable })
      );
    } catch (err) {
      console.log(err);
      dispatch(timetableActions.setIsLoading({ isLoading: false }));
    }
  }, [dispatch, user, isLoggedIn]);

  const fetchMessageSummary = useCallback(async () => {
    if (!isLoggedIn) {
      return;
    }
    try {
      dispatch(dialogueActions.setIsLoading({ isLoading: true }));
      const response = await fetch(`/api/messages/summary/${user.id}`);
      const summaryData = await response.json();
      dispatch(dialogueActions.setIsLoading({ isLoading: false }));
      dispatch(messageSummaryActions.setMessageSummary(summaryData));
    } catch (err) {
      console.log(err);
      dispatch(dialogueActions.setIsLoading({ isLoading: false }));
    }
  }, [isLoggedIn, user.id, dispatch]);

  useEffect(() => {
    fetchMessageSummary();
  }, [fetchMessageSummary]);

  useEffect(() => {
    fetchNews();
  }, [fetchNews]);

  useEffect(() => {
    fetchTimetable();
  }, [fetchTimetable]);

  return (
    <div className={classes["home-grid"]}>
      <NewsGroup
        title={"Новости"}
        news={news.bsuNews}
        isLoading={news.isLoadingNews}
        className={classes["home-news"]}
      />
      <MessageBlock
        className={classes["home-messages"]}
        messages={messageSummary}
      />
      <TodayTimetable
        className={classes["home-timetable"]}
        timetable={timetable}
      />
      <NewsGroup
        title={"Jabber"}
        isLoading={news.isLoadingNews}
        news={news.jabber}
        className={classes["home-jabber"]}
      />
    </div>
  );
};

export default HomeScreen;

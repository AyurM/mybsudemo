import { Fragment } from "react";
import { useSelector } from "react-redux";

import Card from "../ui/Card";
import ButtonLink from "../ui/ButtonLink";
import { MAX_FROM_LENGTH } from "../../util/messages/message-utils";

import classes from "./MessageBlock.module.css";

const getNewAuthorsString = (authors) => {
  let result = "";
  for (let i = 0; i < Math.min(authors.length, MAX_FROM_LENGTH); i++) {
    let user = authors[i];
    result += user.lastName + " " + user.name + " " + user.patronymic + ", ";
  }
  result = result.substring(0, result.length - 2);
  return result;
};

const getAdditionalAuthors = (messages) => {
  if (messages.totalFrom <= messages.from.length) {
    return null;
  }
  return (
    <Fragment>
      {" и еще "}
      {
        <span className={classes.from}>
          {messages.totalFrom - messages.from.length}
        </span>
      }
      {" " + getFromUsersCase(messages.totalFrom - messages.from.length)}
    </Fragment>
  );
};

const getUnreadMessagesCase = (amount) => {
  if (amount >= 11 && amount <= 14) {
    return "непрочитанных сообщений";
  }
  let remainder = amount % 10;
  if (remainder === 1) {
    return "непрочитанное сообщение";
  }
  if (remainder === 2 || remainder === 3 || remainder === 4) {
    return "непрочитанных сообщения";
  }
  return "непрочитанных сообщений";
};

const getFromUsersCase = (amount) => {
  let remainder = amount % 10;
  if (remainder === 1) {
    return "пользователя";
  }
  return "пользователей";
};

const MessageBlock = (props) => {
  const isLoading = useSelector((state) => state.dialogue.isLoading);
  let content;

  if (isLoading) {
    content = <p>Обновление...</p>;
  } else if (props.messages.new === 0) {
    content = <p>У вас нет непрочитанных сообщений</p>;
  } else {
    let additionalAuthorsContent = getAdditionalAuthors(props.messages);
    content = (
      <p>
        У вас{" "}
        <span className={classes["message-number"]}>{props.messages.new}</span>{" "}
        {getUnreadMessagesCase(props.messages.new)} от{" "}
        <span className={classes.from}>
          {getNewAuthorsString(props.messages.from)}
        </span>
        {!!additionalAuthorsContent && additionalAuthorsContent}
      </p>
    );
  }

  return (
    <Card className={`${props.className} ${classes["message-block"]}`}>
      <h2>Сообщения</h2>
      <div className={classes["message-block-container"]}>
        {content}
        {!isLoading && <ButtonLink link="/messages">К сообщениям</ButtonLink>}
      </div>
    </Card>
  );
};

export default MessageBlock;

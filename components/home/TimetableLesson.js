import classes from "./TimetableLesson.module.css";

const getLessonCssClass = (type) => {
  if (!type) {
    return "";
  }
  if (type === "ЛБ") {
    return classes.lab;
  }
  if (type === "ПР") {
    return classes.practice;
  }
  return classes.lecture;
};

const TimetableLesson = ({ lesson }) => {
  return (
    <div className={classes.lesson}>
      <span className={classes["lesson-time"]}>{lesson.time}</span>
      <span className={classes["lesson-subject"]}>{lesson.subject}</span>
      <span
        className={`${classes["lesson-type"]} ${getLessonCssClass(
          lesson.type
        )}`}
      >
        {lesson.type}
      </span>
      <span className={classes["lesson-room"]}>{lesson.room}</span>
      <span className={classes["lesson-groups"]}>
        {lesson.groups ? lesson.groups.join(" ").trim() : lesson.teacherName}
      </span>
    </div>
  );
};

export default TimetableLesson;

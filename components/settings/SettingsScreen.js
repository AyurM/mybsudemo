import { useState } from "react";

import Card from "../ui/Card";

import classes from "./SettingsScreen.module.css";

const SettingsScreen = () => {
  const [darkTheme, setDarkTheme] = useState(
    localStorage.getItem("darkTheme") == 1
  );

  const onDarkThemeChange = (event) => {
    setDarkTheme(event.target.checked);
    localStorage.setItem("darkTheme", event.target.checked ? 1 : 0);
    if (event.target.checked) {
      document.body.className = "dark-theme";
    } else {
      document.body.className = "";
    }
  };

  return (
    <Card className={classes["settings-card"]}>
      <div className={classes["settings-grid"]}>
        <h2>Настройки</h2>
        <div className={classes.container}>
          <label htmlFor="dark-theme" className={classes.text}>
            Темная тема оформления
          </label>
          <input
            type="checkbox"
            id="dark-theme"
            checked={darkTheme}
            onChange={onDarkThemeChange}
            className={classes["toggle-button"]}
          />
        </div>
      </div>
    </Card>
  );
};

export default SettingsScreen;

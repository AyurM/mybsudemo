/* eslint-disable @next/next/no-img-element */
import { useState, useEffect, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";
import Link from "next/link";

import LoginForm from "./LoginForm";
import HelpScreen from "./HelpScreen";
import LoadingIndicator from "../ui/LoadingIndicator";
import { loginWithToken, loginWithLoginAndPassword } from "../../util/auth";

import { authActions } from "../../store/auth-slice";
import { userActions } from "../../store/user-slice";
import classes from "./LoginScreen.module.css";

const LoginScreen = () => {
  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);
  const [isLoading, setIsLoading] = useState(false);
  const [showHelpScreen, setShowHelpScreen] = useState(false);
  const [error, setError] = useState(null);
  const dispatch = useDispatch();
  const router = useRouter();

  const autoLogin = useCallback(async () => {
    if (isLoggedIn) {
      return;
    }
    const token = localStorage.getItem("token");
    if (!token) {
      return;
    }

    setIsLoading(true);
    try {
      const authData = await loginWithToken(token);
      setIsLoading(false);

      router.replace("/").then(() => {
        dispatch(authActions.login({ token: token }));
        dispatch(userActions.setUser(authData.user));
      });
    } catch (err) {
      console.log(err);
      setIsLoading(false);
    }
  }, [dispatch, router, isLoggedIn]);

  useEffect(() => {
    autoLogin();
  }, [autoLogin]);

  const onLoginFormSubmit = async (formData) => {
    console.log(formData);
    setIsLoading(true);
    try {
      const authData = await loginWithLoginAndPassword(formData);
      setIsLoading(false);

      dispatch(authActions.login({ token: authData.token }));
      router.replace("/").then(() => {
        dispatch(userActions.setUser(authData.user));
      });
    } catch (err) {
      console.log(err);
      setError(err.message);
      setIsLoading(false);
    }
  };

  const onHelpClick = () => {
    setShowHelpScreen(true);
  };

  const onHelpScreenClose = () => {
    setShowHelpScreen(false);
  };

  return (
    <div className={classes["login-screen-grid"]}>
      <div className={classes["login-img"]}></div>

      <div className={classes["login-form-container"]}>
        <Link href="https://bsu.ru/">
          <a>
            <div className={classes["logo-container"]}>
              <img src="./images/logo_bsu.png" alt="Логотип БГУ" />
              <span>
                Бурятский государственный университет<br></br>имени Доржи
                Банзарова
              </span>
            </div>
          </a>
        </Link>

        <div className={classes["title-flex"]}>
          <h1>Личный кабинет</h1>
          <button onClick={onHelpClick}>?</button>
        </div>

        {isLoading && (
          <div className={classes.centered}>
            <LoadingIndicator />
          </div>
        )}
        {!isLoading && <LoginForm onSubmit={onLoginFormSubmit} error={error} />}
      </div>

      {showHelpScreen && <HelpScreen onClick={onHelpScreenClose} />}
    </div>
  );
};

export default LoginScreen;

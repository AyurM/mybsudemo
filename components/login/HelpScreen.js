import classes from "./HelpScreen.module.css";

const HelpScreen = ({ onClick }) => {
  const onHelpScreenClick = (event) => {
    event.stopPropagation();
  };

  return (
    <div className={classes["help-screen-overlay"]} onClick={onClick}>
      <div className={classes["help-screen"]} onClick={onHelpScreenClick}>
        <h2>Демо-версия личного кабинета БГУ</h2>
        <p>Список доступных пользователей:</p>
        <ul>
          <li>
            <span>Баиров Виталий Цыренович</span>, студент
          </li>
          <li>
            <span>Гармаева Светлана Бадмаевна</span>, преподаватель
          </li>
          <li>
            <span>Евсеева Елизавета Андреевна</span>, студент
          </li>
          <li>
            <span>Иванов Николай Дмитриевич</span>, преподаватель
          </li>
          <li>
            <span>Кузнецов Евгений Сергеевич</span>, студент
          </li>
          <li>
            <span>Кузьмин Вячеслав Викторович</span>, преподаватель
          </li>
          <li>
            <span>Олейникова Мария Геннадьевна</span>, преподаватель
          </li>
        </ul>
        <p>
          Пароль для всех пользователей: <strong>123</strong>
        </p>

        <button onClick={onClick}>Закрыть</button>
      </div>
    </div>
  );
};

export default HelpScreen;

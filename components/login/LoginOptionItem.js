import classes from "./LoginOptionItem.module.css";

const LoginOptionItem = (props) => {
  const onOptionClick = () => {
    props.onClick(props.option);
  };
  return (
    <div onMouseDown={onOptionClick} className={classes["login-option-item"]}>
      <span className={classes.name}>{props.option.name}</span>
      <span className={classes.position}>{props.option.position}</span>
    </div>
  );
};

export default LoginOptionItem;

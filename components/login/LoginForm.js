import { useRef, useState, useEffect } from "react";

import LoginOptionItem from "./LoginOptionItem";

import classes from "./LoginForm.module.css";

const LoginForm = (props) => {
  const [showLoginOptions, setShowLoginOptions] = useState(false);
  const [loginOptions, setLoginOptions] = useState([]);
  const [userId, setUserId] = useState(null);
  const [login, setLogin] = useState("");
  const passwordInputRef = useRef();
  const rememberInputRef = useRef();

  const onLoginInputChange = (event) => {
    setLogin(event.target.value);
    if (event.target.value.length < 3) {
      setLoginOptions([]);
    }
  };

  useEffect(() => {
    if (login.length < 3) {
      return;
    }
    const timerId = setTimeout(async () => {
      const response = await fetch(`/api/users?startsWith=${login}`);
      const options = await response.json();
      setLoginOptions(options.users);
    }, 500);

    return () => {
      clearTimeout(timerId);
    };
  }, [login]);

  const onLoginOptionClick = (option) => {
    setLogin(option.name);
    setUserId(option.id);
  };

  const onLoginInputFocus = () => {
    setShowLoginOptions(true);
  };

  const onLoginInputBlur = (e) => {
    setShowLoginOptions(false);
  };

  const onFormSubmit = (event) => {
    event.preventDefault();
    const formData = {
      login: userId,
      name: login,
      password: passwordInputRef.current.value,
      remember: rememberInputRef.current.checked,
    };
    props.onSubmit(formData);
  };

  return (
    <form className={classes.form} onSubmit={onFormSubmit}>
      <div className={classes["input-container"]}>
        <label htmlFor="login">Ф.И.О пользователя</label>
        <input
          type="text"
          id="login"
          required
          value={login}
          autoComplete="off"
          onChange={onLoginInputChange}
          onBlur={onLoginInputBlur}
          onFocus={onLoginInputFocus}
        />
        {showLoginOptions && loginOptions.length > 0 && (
          <div className={classes["login-options"]}>
            {loginOptions.map((option) => (
              <LoginOptionItem
                option={option}
                key={option.id}
                onClick={onLoginOptionClick}
              />
            ))}
          </div>
        )}
      </div>
      <div className={classes["input-container"]}>
        <label htmlFor="password">Пароль</label>
        <input type="password" id="password" required ref={passwordInputRef} />
      </div>
      <div className={classes["checkbox-container"]}>
        <input type="checkbox" id="remember-user" ref={rememberInputRef} />
        <label htmlFor="remember-user">Запомнить пользователя</label>
      </div>
      {props.error && <p className={classes.error}>{props.error}</p>}
      <button>Войти</button>
      <span className={classes.link}>Нет личного кабинета?</span>
    </form>
  );
};

export default LoginForm;

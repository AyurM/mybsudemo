/* eslint-disable @next/next/no-img-element */
import { Fragment } from "react";

import NavigationGroup from "./NavigationGroup";
import NavigationItem from "./NavigationItem";

import classes from "./NavigationList.module.css";

const NavigationList = ({ navLinks, mobile }) => {
  const linksWithoutCategory = navLinks
    ? navLinks.find((links) => !links.category)
    : null;

  const linksWithCategory = navLinks
    ? navLinks.filter((links) => links.category)
    : null;

  return (
    <Fragment>
      <ul className={classes["nav-list"]}>
        {linksWithoutCategory &&
          linksWithoutCategory.links.map((item) => (
            <NavigationItem
              key={item.link}
              link={item.link}
              title={item.title}
              icon={item.icon}
              mobile={mobile}
            />
          ))}
      </ul>
      {linksWithCategory &&
        linksWithCategory.map((links) => (
          <NavigationGroup
            category={links.category}
            links={links.links}
            key={links.category}
            mobile={mobile}
          />
        ))}
    </Fragment>
  );
};

export default NavigationList;

/* eslint-disable @next/next/no-img-element */
// import Link from "next/link";

import classes from "./LoggedUserLink.module.css";

const LoggedUserLink = (props) => {
  return (
    <div className={classes["user-link"]}>
      <span>
        {`${props.user.lastName} ${props.user.name}`}
        <br></br>
        {props.user.patronymic}
      </span>
      <img src={props.user.imageUrl} alt="Изображение профиля" />
    </div>
  );
};

export default LoggedUserLink;

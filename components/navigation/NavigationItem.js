/* eslint-disable @next/next/no-img-element */
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";
import Link from "next/link";

import { uiActions } from "../../store/ui-slice";
import classes from "./NavigationItem.module.css";

const NavigationItem = (props) => {
  const router = useRouter();
  const dispatch = useDispatch();

  const onClick = () => {
    if (!props.mobile) {
      return;
    }
    document.body.style.position = "";
    dispatch(uiActions.switchMobileMenu({ show: false }));
  };

  let routeMatch = false;
  if (router.pathname === "/[...slug]") {
    routeMatch = "/" + router.query.slug[0] === props.link;
  } else {
    routeMatch = router.pathname === props.link;
  }

  return (
    <Link href={props.link}>
      <a>
        <div
          className={`${classes["nav-item"]} ${
            routeMatch ? classes.active : ""
          }`}
          onClick={onClick}
        >
          <img src={props.icon} alt="" />
          <span>{props.title}</span>
        </div>
      </a>
    </Link>
  );
};

export default NavigationItem;

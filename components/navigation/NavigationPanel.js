/* eslint-disable @next/next/no-img-element */
import { Fragment, useEffect, useCallback } from "react";
import ReactDOM from "react-dom";
import { useSelector, useDispatch } from "react-redux";
import Link from "next/link";

import NavigationList from "./NavigationList";
import MobileMenu from "./MobileMenu";

import { navLinksActions } from "../../store/nav-links";
import classes from "./NavigationPanel.module.css";

const userRoleToQueryParamValue = new Map([
  ["Преподаватель", "t"],
  ["Студент", "s"],
]);

const NavigationPanel = ({ className }) => {
  const userRole = useSelector((state) => state.user.role);
  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);
  const navLinks = useSelector((state) => state.navLinks.links);
  const dispatch = useDispatch();

  const fetchNavLinks = useCallback(async () => {
    if (!isLoggedIn) {
      return;
    }
    try {
      const response = await fetch(
        `/api/nav-links?role=${userRoleToQueryParamValue.get(userRole)}`
      );
      const links = await response.json();
      console.log(links);
      dispatch(navLinksActions.setLinks({ links: links }));
    } catch (err) {
      console.log(err);
      dispatch(navLinksActions.setLinks({ links: [] }));
    }
  }, [isLoggedIn, userRole, dispatch]);

  useEffect(() => {
    fetchNavLinks();
  }, [fetchNavLinks]);

  const cssClasses = `${classes["nav-panel"]} ${className ? className : ""}`;

  return (
    <Fragment>
      {ReactDOM.createPortal(
        <MobileMenu navLinks={navLinks} />,
        document.getElementById("mobile-menu-portal")
      )}
      <nav className={cssClasses}>
        <Link href="/">
          <a>
            <div className={classes.logo}>
              <img src="./images/logo_bsu.png" alt="Логотип БГУ" />
              <span>
                Бурятский государственный университет имени Доржи Банзарова
              </span>
            </div>
          </a>
        </Link>
        <NavigationList navLinks={navLinks} />
      </nav>
    </Fragment>
  );
};

export default NavigationPanel;

/* eslint-disable @next/next/no-img-element */
import { Fragment } from "react";
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";
import Link from "next/link";

import LoggedUserLink from "./LoggedUserLink";
import HeaderIconLink from "./HeaderIconLink";
import HeaderIconButton from "./HeaderIconButton";

import { authActions } from "../../store/auth-slice";
import { userActions } from "../../store/user-slice";
import { uiActions } from "../../store/ui-slice";
import { dialogueActions } from "../../store/dialogue-slice";
import classes from "./Header.module.css";

const Header = (props) => {
  const router = useRouter();
  const dispatch = useDispatch();

  const cssClasses = `${classes.header} ${
    props.className ? props.className : ""
  }`;

  const onLogout = () => {
    localStorage.removeItem("token");
    router.push("/login").then(() => {
      dispatch(authActions.logout());
      dispatch(userActions.clearUser());
      dispatch(dialogueActions.clearDialogues());
    });
  };

  const onMenuClick = () => {
    document.body.style.position = "fixed";
    dispatch(uiActions.switchMobileMenu({ show: true }));
  };

  return (
    <header className={cssClasses}>
      {props.user.id && (
        <Fragment>
          <HeaderIconButton
            iconPath="./icons/menu.svg"
            onClick={onMenuClick}
            className={classes["mobile-menu"]}
          />
          <div className={classes["header-main-group"]}>
            <HeaderIconLink
              link="/settings"
              iconPath="./icons/settings.svg"
              tooltip="Настройки"
            />
            <HeaderIconLink
              link="/notifications"
              iconPath="./icons/notification.svg"
              tooltip="Уведомления"
            />
            <Link href="/personal-info">
              <a>
                <LoggedUserLink user={props.user} />
              </a>
            </Link>
            <HeaderIconButton
              iconPath="./icons/exit.svg"
              tooltip="Выйти"
              onClick={onLogout}
            />
          </div>
        </Fragment>
      )}
    </header>
  );
};

export default Header;

/* eslint-disable @next/next/no-img-element */
import { useSelector, useDispatch } from "react-redux";
import Link from "next/link";

import NavigationList from "./NavigationList";

import { uiActions } from "../../store/ui-slice";
import classes from "./MobileMenu.module.css";

const MobileMenu = (props) => {
  const showMobileMenu = useSelector((state) => state.ui.showMobileMenu);
  const dispatch = useDispatch();

  const onMenuClose = () => {
    document.body.style.position = "";
    dispatch(uiActions.switchMobileMenu({ show: false }));
  };

  const cssClasses = `${classes["nav-panel"]} ${
    props.className ? props.className : ""
  }`;

  return (
    <div
      className={`${classes["mobile-menu-panel"]} ${
        showMobileMenu ? "" : classes.hide
      }`}
    >
      <nav className={cssClasses}>
        <div className={classes["nav-panel-top"]}>
          <Link href="/">
            <a>
              <div className={classes.logo}>
                <img src="./images/logo_bsu.png" alt="Логотип БГУ" />
                <span>БГУ им. Доржи Банзарова</span>
              </div>
            </a>
          </Link>

          <button className={classes["close-button"]} onClick={onMenuClose}>
            <span>Закрыть</span>
            <img src="./icons/close.svg" alt="" />
          </button>
        </div>

        <NavigationList navLinks={props.navLinks} mobile />
      </nav>
    </div>
  );
};

export default MobileMenu;

/* eslint-disable @next/next/no-img-element */
import { useState, useRef, useEffect } from "react";

import classes from "./HeaderIconButton.module.css";

const tooltipDelay = 400;

const HeaderIconButton = (props) => {
  const [timeoutId, setTimeoutId] = useState(null);
  const [showTooltip, setShowTooltip] = useState(false);
  const isUnmounted = useRef(false);

  const onButtonClick = () => {
    props.onClick();
  };

  const onMouseEnter = () => {
    if (!props.tooltip) {
      return;
    }
    setTimeoutId(
      setTimeout(() => {
        if (!isUnmounted.current) {
          setShowTooltip(true);
        }
      }, tooltipDelay)
    );
  };

  const onMouseLeave = () => {
    if (!props.tooltip) {
      return;
    }
    setShowTooltip(false);
    if (timeoutId) {
      clearTimeout(timeoutId);
      setTimeoutId(null);
    }
  };

  //Предотвращает попытку отрисовать tooltip кнопки
  //на уже удаленном компоненте
  useEffect(() => {
    return () => {
      isUnmounted.current = true;
    };
  }, []);

  return (
    <button
      className={`${classes["header-button"]} ${
        props.className ? props.className : ""
      }`}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      onClick={onButtonClick}
    >
      <img src={props.iconPath} alt="" />
      {showTooltip && <div className={classes.tooltip}>{props.tooltip}</div>}
    </button>
  );
};

export default HeaderIconButton;

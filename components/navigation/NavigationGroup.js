/* eslint-disable @next/next/no-img-element */
import { useState } from "react";

import NavigationItem from "./NavigationItem";

import classes from "./NavigationGroup.module.css";

const NavigationGroup = (props) => {
  const [opened, setOpened] = useState(true);

  const onTitleClick = () => {
    setOpened((prevOpened) => {
      return !prevOpened;
    });
  };

  return (
    <div className={classes["nav-group"]}>
      <div className={classes["nav-group-title"]} onClick={onTitleClick}>
        <h3>{props.category}</h3>
        <img
          className={`${classes.arrow} ${
            opened ? classes["group-opened"] : ""
          }`}
          src="./icons/arrow-down-sign-to-navigate.svg"
          alt=""
        />
      </div>

      {opened && (
        <ul className={classes["nav-list"]}>
          {props.links.map((item) => (
            <NavigationItem
              key={item.link}
              link={item.link}
              title={item.title}
              icon={item.icon}
              mobile={props.mobile}
            />
          ))}
        </ul>
      )}
    </div>
  );
};

export default NavigationGroup;

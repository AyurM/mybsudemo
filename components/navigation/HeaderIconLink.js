/* eslint-disable @next/next/no-img-element */
import { useState } from "react";
import Link from "next/link";

import classes from "./HeaderIconLink.module.css";

const tooltipDelay = 400;

const HeaderIconLink = (props) => {
  const [timeoutId, setTimeoutId] = useState(null);
  const [showTooltip, setShowTooltip] = useState(false);
  const onMouseEnter = () => {
    setTimeoutId(
      setTimeout(() => {
        setShowTooltip(true);
      }, tooltipDelay)
    );
  };
  const onMouseLeave = () => {
    setShowTooltip(false);
    if (timeoutId) {
      clearTimeout(timeoutId);
      setTimeoutId(null);
    }
  };

  return (
    <Link href={props.link}>
      <a
        className={classes["header-link"]}
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
      >
        <img src={props.iconPath} alt="" />
        {showTooltip && <div className={classes.tooltip}>{props.tooltip}</div>}
      </a>
    </Link>
  );
};

export default HeaderIconLink;
